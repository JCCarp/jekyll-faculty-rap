---
layout: default
---

## Quickstart Guide

Click [here](https://bitbucket.org/UBCCS/jekyll-faculty/src/master/) to return to the 
repository website that shows instructions on how to use
and customize this UBC CS faculty and course static website generator yourself.  Browse
the markdown files in particular (```.md``` file extension; start with ```index.md``` -- make sure you view the raw source).

Browse the remainder of this demonstration website to gain a sense of what can
be easily and efficiently generated and maintained (like $x^2 + 1 = 17$, ```while(true) ++i```, and the references below).

---------------
## About Me

<img class="profile-picture" src="headshot.jpg">

I'm an Associate Professor and the Associate Head for the Computer Science Undergraduate Program at the Computer Science Department at the University of British Columbia (in Vancouver).


## Research Interests

I am affilated with the Data Management and Mining group.
My research centers on (1) how data can be managed in situations where there are multiple databases and (2) how to manage data that is currently not well supported by databases. To that end, my students and I are currently exploring a number of topics, including:

Making sense of data that is stored in relational databases or XML is difficult. For example, if civil engineers are trying to extract information about where two pieces of a building intersect, they may need to find 10 different elements in a schema that contains thousands of options. This project seeks to allow users to understand their schemas well enough to query them. This is joint work with Zainab Zolaktaf.
In many cases where analysis is being performed, a user may have an aggregation query to which she knows what the correct answer should be for one case. Trying to determine why the answer that the user is getting is different from the one provided by the "Oracle" is a frustrating and error-prone process. This project seeks to allow users to get feedback to why their aggregation queries are not providing the answer that they expect. This is joint work with Omar AlOmeir.
Information about some of these topics can be found by looking at my publications.

## Selected Publications

{% bibliography --file selected.bib %}
